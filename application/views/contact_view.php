<div class="content">
    <div class="container">

        <div class="page-header">
            <h1>Contact us</h1>
        </div>
        <div class="row-fluid">
            <!-- Start: CONTACT US FORM -->
            <div class="span4 offset1">
                <div class="page-header">
                    <h2>Quick message</h2>
                </div>

                <?php echo form_open('contact/emailsender'); ?>
                <div class="control-group">
                    <div class="controls">

                        <input type="text" id="sender_name" name="sender_name" placeholder="From">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">

                        <input type="text" id="sender_email" name="sender_email" placeholder="Email">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">

                        <input type="text" id="subject" name="subject" placeholder="Subject">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <textarea id="message" name="message" placeholder="Message"></textarea>

                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">

                        <input type="submit" name="submit" class="btn btn-primary btn-large" value="Send">
                    </div>
                </div>
                <?php echo form_close(); ?>
                <script type="text/javascript">
                    $(function() {
                        $('input[type=text], textarea').focus(function() {
                            $(this).val('');
                        });
                    });
                </script>
            </div>
            <!-- End: CONTACT US FORM -->
            <!-- Start: OFFICES -->
            <div class="span5 offset1">
                <div class="page-header">
                    <h2>Offices</h2>
                </div>
                <h3>India</h3>
                <div>
                    <address class="pull-left">
                        <strong>ThinkIT, Inc.</strong><br>
                        Margao, Goa<br>
                        India<br>
                    </address>
                    <div class="pull-right">
                        <div class="bottom-space">
                            <i class="icon-phone icon-large"></i> (123) 123-1234
                        </div>
                        <a href="mailto:contact@bootbusiness.com" class="contact-mail">
                            <i class="icon-envelope icon-large"></i>
                        </a> contact@bootbusiness.com
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- End: OFFICES -->
        </div>
    </div>
</div>

